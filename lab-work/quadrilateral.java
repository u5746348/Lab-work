import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.Scanner;

/**
 * Created by u5746348 on 2017/3/4.
 */
public class quadrilateral {
    public static void main(String[] args) {
        Scanner integer = new Scanner(System.in);
        int n = integer.nextInt();
        if (n%2==0){
            System.out.println(n+" is not an odd number.");
        }
        else{
            if (3<=n&&n<=15) {
                for (int i=0;i<n;i++) {
                    int j =Math.round(n/2);
                    if (i<j || i>j) {
                        for (int l=0; l<(Math.abs(j-i));l++) {
                            System.out.print(" ");
                        }
                        for (int l =0; l< n-2*(Math.abs(j-i));l++) {
                            System.out.print("#");
                        }
                    }
                    else{
                        for (int l = 0; l< n; l++) {
                            System.out.print("#");
                        }
                    }
                    System.out.println(" ");
                }
            }
            else{
                System.out.println(n+" is not in the interval [3,15].");
            }
        }
    }
}
