/**
 *  * Created by u5746348 on 2017/3/4.
 */
public class DemoTextDraw2 {
    public static void main(String[] args) {
        Shape box = new Box2();
        Shape tri = new Triangle2();
        box.draw();
        System.out.println("------------------");
        tri.draw();
    }
}
/**
 *  * In abstract class keyword ‘abstract’ is mandatory to declare a method as an abstract,however in an interface, it is optional.
 */