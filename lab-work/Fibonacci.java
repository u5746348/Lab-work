import java.util.Scanner;

/**
 * Created by u5746348 on 2017/3/4.
 */
public class Fibonacci {
    public static void main(String[] args) {
        Scanner integer = new Scanner(System.in);
        int n = integer.nextInt();
        System.out.println(fibonacci(n));
    }
    public static Integer fibonacci(int i){
        if(i<0){
            System.out.print(i+" is not a valid number ");
            return null;
        }
        else if(i==0){
            return 0;
        }
        else if(i==1){
            return 1;
        }
        else
            return fibonacci(i-1)+fibonacci(i-2);
    }
}
